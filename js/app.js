$(document).ready(function () {
  'use strict'
  var app  = {}

  // CONFIG
  app.name = 'MedNote'
  app.version = 1
  app.db   = new Dexie(app.name.split(' ').join('-'))
  app.utils= {}
  app.model= {
    patient: {},
  }
  app.menu = [
    {id: 'patient', label: 'Patient'},
    {id: 'visit', label: 'Visit'}]


  // APP
  app.utils.render = function (tpl, el, data) {
    var template = Handlebars.compile($(tpl).html())
    $(el).html(template(data))
  }

  app.utils.collectFieldNames = function (model, cb) {
    var fields = []
    for (var f in app.model[model].fields) 
      fields.push(app.model[model].fields[f].name)
    cb && cb (fields)
  }

  app.utils.getForm = function (model, cb) {
    var obj = {}
    app.utils.collectFieldNames(model, function (fieldNames) {
      for (var f in fieldNames) obj[fieldNames[f]] = $('[name="'+fieldNames[f]+'"]').val()
      cb && cb (obj)
    })
  }

  app.utils.setForm = function (model, obj) {
    app.utils.collectFieldNames(model, function (fieldNames) {
      for (var f in fieldNames) $('[name="'+fieldNames[f]+'"]').val(obj[fieldNames[f]])
    })
  }

  app.utils.confirm = function (cb) {
    Materialize.toast('Are you sure?\
    <a class="waves-effect waves-light btn btn-unconfirm red">NO</a>\
    <a class="waves-effect waves-light btn btn-confirm">YES</a>', 4000)
    $('.btn-unconfirm').click(function () {
      var toastElement = $('.toast').first()[0]
      var toastInstance = toastElement.M_Toast
      toastInstance.remove()
    })
    $('.btn-confirm').click(function () {
      $('.btn-unconfirm').click()
      cb && cb()
    })
  }

  app.init = function (cb) {
    app.utils.render($('#navbar-tpl'), $('#navbar'), {
      appName: app.name,
      menu: app.menu
    })
    $('.button-collapse').sideNav({closeOnClick: true})

    for (var model in app.model) {
      app.utils.collectFieldNames (model, function (fields) {
        app.db.version(app.version).stores({
          patient: '++id,' + fields.join(',')
        })
      })
    }

    cb && cb()
  }




  // ENTITY: PATIENT
  app.model.patient.fields = [
    {name: 'name', label: 'Name'},
    {name: 'room', label: 'Room'},
    {name: 'bed',  label: 'Bed'}]

  app.model.patient.list = function () {
    var item = []
    app.db.patient.each(function (p) {
      item.push({id: p.id, label: p.name})
    })
    .then(function () {
      app.utils.render($('#list-tpl'), $('.section'), {
        list: 'Pasien',
        item: item
      })
      var template = Handlebars.compile($('#floating-btn-tpl').html())
      $('.section').append(template({id: 'btn-add-patient', icon: 'add'}))
      $('#btn-add-patient').click(app.model.patient.add)
      $('.collection-item').click(function () {
        app.model.patient.edit($(this).attr('data-id'))
      })
    })
  }

  app.model.patient.add = function () {
    app.utils.render($('#form-tpl'), $('.section'), {
      form: 'Pasien',
      input: app.model.patient.fields,
      back: true
    })
    $('.btn-back').click(app.model.patient.list)
    $('.btn-save').click(app.model.patient.create)
  }

  app.model.patient.create = function () {
    app.utils.getForm('patient', function (patient) {
      app.db.patient.add(patient)
      .then(app.model.patient.list)
    })
  }

  app.model.patient.edit = function (id) {
    id = parseInt(id)
    app.db.patient.get(id, function (patient) {
      app.utils.render($('#form-tpl'), $('.section'), {
        form: 'Pasien',
        input: app.model.patient.fields,
        back: true
      })
      app.utils.setForm('patient', patient)
      $('.btn-back').click(app.model.patient.list)
      $('.btn-save').click(function () {
        app.model.patient.update(patient.id)
      })
      var template = Handlebars.compile($('#floating-btn-tpl').html())
      $('.card-content').prepend(template({id: 'btn-delete-patient', icon: 'delete'}))
      $('#btn-delete-patient').click(function () {
        app.utils.confirm(function () {
          app.model.patient.remove(id)
        })
      })
    })
  }

  app.model.patient.update = function (id) {
    app.utils.getForm('patient', function (patient) {
      patient.id = id
      app.db.patient.put(patient)
      .then(app.model.patient.list)
    })
  }

  app.model.patient.remove = function (id) {
    app.db.patient.where('id').equals(id).delete().then(app.model.patient.list)
  }



  // APP.RUN
  app.init(function () {
    app.model.patient.list()
    $('.menu-patient').click(app.model.patient.list)
  })
})